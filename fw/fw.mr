using uart
using vect

func read_fast(mmsb, msb, lsb)
  @PORTC <- lsb
  @PORTL <- msb
  @PORTD <- mmsb
  return @PINA
end

func read_all()
  @PORTB <- PORTB2
  for mmsb <- 0 to 0b111 do
    for msb <- 0 to 0xFF do
      for lsb <- 0 to 0xFF do
        uart_send(read_fast(mmsb, msb, lsb))
      done
    done
  done
  @PORTB <- PORTB0
end

func read_header()
  @PORTB <- PORTB2
  for lsb <- 0xF0 to 0xFF do
    uart_send(read_fast(0, 0x7F, lsb))
  done
  @PORTB <- PORTB0
end

func impulse()
   @PORTB <- PORTB1
   @PORTB <- PORTB2 PORTB1
end

func command(vect : instr)
   var seq <- none

   @DDRA <- 0xFF

   for i <- 0 to vect_len(instr) - 1 do
     seq <- (vect : instr[i])
     @PORTA <- seq[0]
     @PORTC <- seq[1]
     @PORTD <- seq[2]
     @PORTL <- seq[3]
     impulse()
   done

   @DDRA <- 0
end

func write_flash(vect : instr)
  var seq <- none
  for mmsb <- 0 to 0b111 do
    for msb <- 0 to 0xFF do
      for lsb <- 0 to 0xFF do
        seq <- (vect : instr[3])
        seq[1] <- lsb
        seq[2] <- mmsb
        seq[3] <- msb
	waitfor not uart_available()
        seq[0] <- uart_recv()
        command(instr)
      done
    done
  done
end

setup
  var ret <- 0
  var write_instr <- {{0xAA; 0x55; 0x0; 0x5};   \
                      {0x55; 0xAA; 0x0; 0x2};   \
                      {0xA0; 0x55; 0x0; 0x5};   \
                      {0x0; 0x0; 0x0; 0x0}}
  var erase_instr <- {{0xAA; 0x55; 0x0; 0x5};   \
                      {0x55; 0xAA; 0x0; 0x2};   \
                      {0x80; 0x55; 0x0; 0x5};   \
                      {0xAA; 0x55; 0x0; 0x5};   \
                      {0x55; 0xAA; 0x0; 0x2};   \
                      {0x10; 0x55; 0x0; 0x5}}

  @DDRA <- 0            # Data
  @DDRB <- 0xFF         # ~WE ~OE ~CE
  @DDRC <- 0xFF         # A7:A0
  @DDRD <- 0xFF         # A18:A16
  @DDRL <- 0xFF         # A15:A8

  @PORTC <- 0
  @PORTL <- 0
  @PORTB <- PORTB0

  uart_init(16)
end

loop
  if uart_available() then
    ret <- uart_recv()
    if ret = 'r' then
      uart_send(42)
      read_all()
    elif ret = 'h' then
      read_header()
    elif ret = 'w' then
      uart_send(42)
      write_flash(write_instr)
    elif ret = 'e' then
      uart_send(42)
      command(erase_instr)
      @PORTB <- PORTB2
      waitfor read_fast(0, 0, 0) <> 0xFF
      @PORTB <- PORTB0
      uart_send(42)
    else
      uart_send(0)
    endif
  endif
end
