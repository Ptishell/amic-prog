DEV ?= /dev/ttyUSB0

all::
	$(MAKE) -C fw

flash::
	$(MAKE) -C fw flash

clean::
	$(MAKE) -C fw clean
