#! /usr/bin/env python2

import serial
import time
import argparse
import sys
import os

def check_header(com):
    if com.read() != "*":
        sys.stderr.write("Bad header\n")
        exit(2)

def parse_args():
    parser = argparse.ArgumentParser(description= \
                                    "AMIC A29040C Programmer")
    parser.add_argument('order',                        \
                            choices=['read', 'write', 'erase'],  \
                            help="""
dump : dump the flash data to the output file (specified with -o option)
""")
    parser.add_argument('-d', '--device',               \
                            metavar='DEV',              \
                            type=str,                   \
                            nargs=1,                    \
                            default=["/dev/ttyUSB0"],   \
                            help="set device (default: %(default)s)")
    parser.add_argument('-o',                           \
                            metavar='OUTPUT',           \
                            type=str,                   \
                            nargs=1,                    \
                            default=["out.bin"],        \
                            help="set output file (default: %(default)s)")
    parser.add_argument('-i',                           \
                            metavar='INPUT',            \
                            type=str,                   \
                            nargs=1,                    \
                            default=["in.bin"],         \
                            help="set output file (default: %(default)s)")
    parser.add_argument('-s',                           \
                            metavar='SIZE',             \
                            type=int,                   \
                            nargs=1,                    \
                            default=[512],               \
                            help="set size (default: %(default)s)")

    return vars(parser.parse_args())


def connect_mcu(device):
    sys.stdout.write("Connecting to programmer ({0})...".format(device))
    try:
        com = serial.Serial(device, 115200)
        sys.stdout.write("\tDone\n")
    except serial.serialutil.SerialException:
        sys.stdout.write("\tFailure !\n")
        exit(2)
    time.sleep(2)
    return com

def progress(f, n):
    for x in range(n):
        sys.stdout.write("\r{0}% ({1} B / {2} B)".format(((x + 1) * 100) /      \
                                                             n,                 \
                                                             (x + 1),           \
                                                             n))
        sys.stdout.flush()
        f(x)
    sys.stdout.write("\n")

def erase_flash(com):
    com.write('e')
    check_header(com)
    sys.stdout.write("Erasing cartridge data...")
    sys.stdout.flush()
    check_header(com)
    sys.stdout.write("\t\t\tDone\n")

def read_flash(com, path, size):
    f = open(path, "wb")
    com.write('r')
    check_header(com)
    print("Reading cartridge data to {0}...".format(path))
    progress(lambda x : f.write(com.read(1)), size * 1024)
    f.close()

def write_flash(com, path, header):
    size = os.path.getsize(path)
    if size > (512 * 1024):
        sys.stderr.write("File must be <= 512kB\n")
        exit(2)
    f = open(path, "rb")
    com.write('w')
    check_header(com)
    time.sleep(2)
    sys.stdout.write("Writing cartridge data from {0}...\n".format(path))
    progress(lambda x : com.write(f.read(1)), size)
    f.close()

def main():
    args = parse_args()
    com = connect_mcu(args['device'][0])
    header = []
    if args["order"] in ["erase", "write"]:
        erase_flash(com)
    if args["order"] == "read":
        read_flash(com, args['o'][0], args['s'][0])
    elif args["order"] == "write":
        write_flash(com, args['i'][0], header)
    sys.stdout.write("Done\n")

if __name__ == "__main__":
    main()
